package de.uni_passau.fim.information_extraction.simple_graphene;

import de.uni_passau.fim.information_extraction.simple_graphene.utility.FileIO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.UUID;

/**
 * Small test case for the CLI of SimpleRelationExtraction
 *
 * Created by bernhard on 29.07.16.
 */
public class SimpleRelationExtractionCLITest {

    private String simplifiedSingleSentenceGold;

    @Before
    public void setUp() throws Exception {
        simplifiedSingleSentenceGold =
                String.join("\n",
                    FileIO.readFile(
                        new File(
                                SimpleRelationExtractionCLITest.class.getResource("/SimplifiedSingleSentence.fixture").getFile())));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSimplifySingleSentence() throws Exception {

        File outfile =
                File.createTempFile("SimpleRelationExtraction_test", UUID.randomUUID().toString());

        String[] commands = {
                "-t", "SIMPLIFY",
                "-s", "In 2016, Bernhard finished his master's thesis at the University Passau in Germany.",
                "-o", outfile.getAbsolutePath()
        };

        SimpleRelationExtractionCLI.main(commands);

        String createdFile = String.join("\n", FileIO.readFile(outfile));

        Assert.assertEquals(simplifiedSingleSentenceGold, createdFile);

        //noinspection ResultOfMethodCallIgnored
        outfile.delete();
    }
}